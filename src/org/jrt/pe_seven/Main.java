/**
 * org.jrt.pe_seven is a list of operations to complete project euler problem seven
 */
package org.jrt.pe_seven;

import java.math.BigInteger;

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, 
 * we can see that the 6th prime is 13.
 * 
 * What is the 10 001st prime number?
 * 
 * @author jrtobac
 *
 */
public class Main {

	public static void main(String[] args) {

		BigInteger num = new BigInteger("1");
		int count = 0;
		
		while(count < 10001) {
			num = num.nextProbablePrime();
			count++;
		}
		
		System.out.println(num);
		
	}

}
